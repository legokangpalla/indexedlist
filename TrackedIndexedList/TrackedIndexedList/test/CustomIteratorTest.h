// TrackedIndexedList.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <iostream>
#include <string>
#include <queue>
#include <functional>
#include "../TrackedIndexedList.h"
#include "../catch.hpp"
#include <boost/container/stable_vector.hpp>
#include <boost/variant/recursive_wrapper.hpp>
#include <boost/container/deque.hpp>
#include <chrono>  // for high_resolution_clock
#include <array>
#include "RandomAccessIteratorBase.h"
#include "../IteratorWrapper.h"

//#include "../IteratorWrapper.h"

namespace CustomIteratorTest
{
	class Mesh;

	class HalfEdgeItr : public RandomAccessIteratorBase<HalfEdgeItr, Mesh>
	{
	public:
		using RandomAccessIteratorBase::RandomAccessIteratorBase;
		HalfEdgeItr next()const;
		HalfEdgeItr prev()const;
		void moveNext();
		void movePrev();
	};

	class HalfEdgeConstItr : public HalfEdgeItr
	{
	public:
		using HalfEdgeItr::HalfEdgeItr;
	};
	struct HalfEdge
	{
		uint32_t next;
		uint32_t prev;
		uint32_t from;
		uint32_t to;
		uint32_t owningFace;
	};
	struct MeshFace {
		uint32_t edge;
	};
	struct MeshVertex {
		std::vector<uint32_t> leavingEdges;
		std::vector<uint32_t> arrivingEdges;
	};
	class MeshIteratorFactory
	{
	public:
		MeshIteratorFactory() :_mesh(nullptr) {}
		MeshIteratorFactory(Mesh* mesh) : _mesh(mesh)
		{}
	protected:
		Mesh* _mesh;
	};
	class HalfEdgeItrFactory : private MeshIteratorFactory
	{
	public:
		typedef HalfEdgeItr iterator;
		typedef HalfEdgeConstItr const_iterator;
		typedef IndexedList<HalfEdge, std::allocator<HalfEdge>, HalfEdgeItrFactory> containerType;
		using MeshIteratorFactory::MeshIteratorFactory;
		iterator buildIterator(size_t index, const containerType* containerPtr)const
		{
			return HalfEdgeItrFactory::iterator(index, _mesh);

		}
		const_iterator buildConstIterator(size_t index, const containerType* containerPtr)const
		{
			return HalfEdgeItrFactory::const_iterator(index, _mesh);
		}
	};


	class Mesh
	{
	public:
		Mesh() :_edges(HalfEdgeItrFactory(this))
		{}
		TrackedIndexedList<HalfEdge, std::allocator<HalfEdge>, HalfEdgeItrFactory> _edges;
	};



	//MeshDataIterator::MeshDataIterator()
	//{

	//}
	//MeshDataIterator::MeshDataIterator(size_t idx, Mesh* owner) : RandomAccessIteratorBase<MeshDataIterator, Mesh>(idx, owner)
	//{
	//}



	TEST_CASE("circ dep2", "[CustormIteratorTest]") {



		SECTION("test") {
			Mesh m;
			auto constEdge = m._edges.cbegin();
			auto jacs = constEdge + 1;
			auto edge = m._edges.toNormItr(constEdge);

		}
		//SECTION("test2") {
		//	IndexedList<B> list;
		//	list.push_back(B());
		//	auto& b = list.back();
		//	b.w = new auto(list.end() - 1);
		//	auto num = b.f();
		//	REQUIRE(num == 3);
		//}

		//SECTION("comp") {
		//	TrackedIndexedList<int> list{ 1, 2, 3, 4, 5 };
		//	auto normItr = list.begin() + 2;
		//	auto test = list.begin() + 1;
		//	auto wrappedItr = wrapIterator(list.begin() + 1);
		//	REQUIRE(*wrappedItr == 2);
		//	REQUIRE(*wrappedItr == *test);
		//	REQUIRE(wrappedItr == test);
		//	REQUIRE(wrappedItr.getItr() == test);

		//}
		//SECTION("copy") {
		//	TrackedIndexedList<int> list{ 1, 2, 3, 4, 5 };
		//	auto itr0 = list.begin();
		//	auto itr1 = itr0;
		//	itr0 = itr0 + 3;
		//	auto wrappedItr = wrapIterator(list.begin() + 1);
		//	auto test = wrappedItr;
		//	auto test2(wrappedItr);
		//	REQUIRE(*wrappedItr == *test);
		//	REQUIRE(wrappedItr == test);
		//	REQUIRE(test2 == test);
		//	wrappedItr.getItr() = wrappedItr.getItr() + 5;
		//	REQUIRE(wrappedItr != test);
		//	REQUIRE(wrappedItr != test2);

		//}
	}

}