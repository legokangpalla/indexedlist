// TrackedIndexedList.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <iostream>
#include <string>
#include <queue>
#include <functional>
#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"

#include "test/IndexedListTest.h"
#include "test/TrackedIndexedListTest.h"
#include "test/CustomIteratorTest.h"


