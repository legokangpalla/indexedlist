# IndexedList

STL style std::deque adapter that have:

	1. Random access(O(1))
	
	2. Random delete(with swap, O(1))
	
	3. Valid reference to elements until they are deleted(no move after insertion/deletion)
	
	4. Valid iterators until element is deleted(no move after insertion/deletion, no iterator state changes either)
	
	5. Simple function pointer event for notifying element swap

# TrackedIndexedList

IndexedList subclass that have:

	1. Tracks all the changes made(non const access, deletion, insertion)
	
	2. Flushes change history (useful for making a patch)
